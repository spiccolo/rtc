/*==================[inclusions]=============================================*/
#include "main.h"

#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "board.h"

#include "task.h"
#include "semphr.h"
#include "rtc.h"

#include "ciaaUART.h"


/*==================[macros and definitions]=================================*/
#define MSG_SIZE 256

/*==================[internal data declaration]==============================*/

typedef struct TASK_DATA {
	RTC_TIME_T		FullTime;
	bool flag;
	float amp;
} TASK_DATA_T;

/*==================[internal functions declaration]=========================*/

static void initHardware(void);

/*==================[internal data definition]===============================*/
static TASK_DATA_T	dato;
/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void initHardware(void)
{
	Board_Init();
	SystemCoreClockUpdate();
	ciaaUARTInit();
	//	Chip_RTC_Init(LPC_RTC);
}

static void initTasksData(void)
{

	//	dato.FullTime.time[RTC_TIMETYPE_SECOND] = 25;
	//	dato.FullTime.time[RTC_TIMETYPE_MINUTE] = 30;
	//	dato.FullTime.time[RTC_TIMETYPE_HOUR] = 14;
	//	dato.FullTime.time[RTC_TIMETYPE_DAYOFMONTH] = 6;
	//	dato.FullTime.time[RTC_TIMETYPE_MONTH] = 12;
	//	dato.FullTime.time[RTC_TIMETYPE_YEAR] = 2019;
	//
	//	Chip_RTC_SetFullTime(LPC_RTC, &(dato.FullTime));
	//
	//	/* Enable RTC (starts increase the tick counter and second counter register) */
	//	Chip_RTC_Enable(LPC_RTC, ENABLE);

	dato.amp=0;
	dato.flag=false;
}


void task_uart (void *data)
{
	TASK_DATA_T *pdato = (TASK_DATA_T *)data;
	char buffer[MSG_SIZE];

	while (1) {
		vTaskDelay(1000);
		/*get actual time, save in (dato.FullTime)*/
		Chip_RTC_GetFullTime(LPC_RTC, &(pdato->FullTime));

		sprintf (buffer, "Time: %.2d:%.2d:%.2d %.2d/%.2d/%.4d\r\n", pdato->FullTime.time[RTC_TIMETYPE_HOUR],
				pdato->FullTime.time[RTC_TIMETYPE_MINUTE],
				pdato->FullTime.time[RTC_TIMETYPE_SECOND],
				pdato->FullTime.time[RTC_TIMETYPE_MONTH],
				pdato->FullTime.time[RTC_TIMETYPE_DAYOFMONTH],
				pdato->FullTime.time[RTC_TIMETYPE_YEAR]);

		Chip_UART_SendBlocking(LPC_USART2, buffer, strlen(buffer));
	}
}



void task_setrtc (void *data)
{
	TASK_DATA_T *pdato = (TASK_DATA_T *)data;
	char buffer[MSG_SIZE];

	while (1) {
		if (Buttons_GetStatus()!=NO_BUTTON_PRESSED){
			vTaskDelay(1000);
			if (Buttons_GetStatus()!=NO_BUTTON_PRESSED){
				sprintf (buffer, "Comienzo de seteo de RTC\r\n\n");
				Chip_UART_SendBlocking(LPC_USART2, buffer, strlen(buffer));
				blink_led();
				seteo_rtc(pdato);
				sprintf (buffer, "El RTC ha sido seteado\r\n\n");
				Chip_UART_SendBlocking(LPC_USART2, buffer, strlen(buffer));

			}
		}
	}
}

void task_led (void *data)
{
	while (1) {
		Board_LED_Toggle(0);
		vTaskDelay(300);
	}
}

/*==================[external functions definition]==========================*/

int main(void)
{

	initHardware();

	initTasksData();


	xTaskCreate(task_uart, (const char *)"taskUART",
			1024, &dato, tskIDLE_PRIORITY+1, NULL);

	xTaskCreate(task_setrtc, (const char *)"taskUART",
			1024, &dato, tskIDLE_PRIORITY+1, NULL);


	xTaskCreate(task_led, (const char *)"taskled",
			configMINIMAL_STACK_SIZE*2, NULL, tskIDLE_PRIORITY+1, 0);

	vTaskStartScheduler();

	while (1) {
	}
}

/*==================[end of file]============================================*/
