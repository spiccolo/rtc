/*==================[inclusions]=============================================*/
#include "main.h"
#include "board.h"
#include "rtc.h"


/*==================[macros and definitions]=================================*/
/*==================[internal data declaration]==============================*/

typedef struct TASK_DATA {
	RTC_TIME_T		FullTime;
	bool flag;
	float amp;
} TASK_DATA_T;

/*==================[internal functions declaration]=========================*/
/*==================[internal data definition]===============================*/
/*==================[external data definition]===============================*/
/*==================[internal functions definition]==========================*/

void seteo_rtc (void * data)
{
	TASK_DATA_T *pdato = (TASK_DATA_T *)data;

	/*Change this values for rtc setting. We recommend set a nearly future time and press the button at that time*/
	Chip_RTC_Init(LPC_RTC);

	pdato->FullTime.time[RTC_TIMETYPE_SECOND] = 8;
	pdato->FullTime.time[RTC_TIMETYPE_MINUTE] = 9;
	pdato->FullTime.time[RTC_TIMETYPE_HOUR] = 10;
	pdato->FullTime.time[RTC_TIMETYPE_DAYOFMONTH] = 11;
	pdato->FullTime.time[RTC_TIMETYPE_MONTH] = 12;
	pdato->FullTime.time[RTC_TIMETYPE_YEAR] = 2019;

	Chip_RTC_SetFullTime(LPC_RTC, &(pdato->FullTime));
	/* Enable RTC (starts increase the tick counter and second counter register) */
	Chip_RTC_Enable(LPC_RTC, ENABLE);
}

void blink_led(void)
{
	for (int i = 0; i < 4; i++) {
		Board_LED_Toggle(5);
		vTaskDelay(700);
		Board_LED_Toggle(4);
		vTaskDelay(700);
		Board_LED_Toggle(3);
		vTaskDelay(700);
	}
}

/*==================[external functions definition]==========================*/
/*==================[end of file]============================================*/
